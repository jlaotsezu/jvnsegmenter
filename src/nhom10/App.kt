package nhom10

object PreProcessing{
    @JvmStatic
    fun main(args: Array<String>){
        val rawTextConverter = InjectorUtil.getInstance().fromJvnSegmenterToRawTextConverter()
        val jvnSegmenterOutputConverter = InjectorUtil.getInstance().fromTeacherToJvnSegmenterOutputConverter()
        val fileReader = InjectorUtil.getInstance().getFileReader()
        val fileWriter = InjectorUtil.getInstance().getFileWriter()

        for(fileName in FILES_NAME){
            val teacherSegmentedFileFullPath = "$TEACHER_FILES_PATH$PATH_SEPARATOR$fileName.$TEACHER_FILES_EXTENSION"
            val rawTextFileFullPath = "$JVNSEGMENTER_FILES_PATH$PATH_SEPARATOR$fileName.$JVNSEGMENTER_INPUT_FILES_EXTENSION"
            val expectedOutputFileFullPath = "$TEACHER_FILES_PATH$PATH_SEPARATOR$fileName.$JVNSEGMENTER_OUTPUT_FILES_EXTENSION"

            val teacherLines = fileReader.read(teacherSegmentedFileFullPath)

            val expectedOutputLines = jvnSegmenterOutputConverter.convert(teacherLines)
            val rawTextLines = rawTextConverter.convert(teacherLines)

            fileWriter.writeLines(rawTextLines, rawTextFileFullPath)
            fileWriter.writeLines(expectedOutputLines, expectedOutputFileFullPath)
        }
    }
}
object Evaluating{
    @JvmStatic
    fun main(args: Array<String>){
        val evaluator = InjectorUtil.getInstance().getEvaluator()
        val fileReader = InjectorUtil.getInstance().getFileReader()

        var totalScore = 0f
        val fileAmount = FILES_NAME.size

        for(fileName in FILES_NAME){
            val actualOutputFileFullPath = "$JVNSEGMENTER_FILES_PATH$PATH_SEPARATOR$fileName.$JVNSEGMENTER_OUTPUT_FILES_EXTENSION"
            val expectedOutputFileFullPath = "$TEACHER_FILES_PATH$PATH_SEPARATOR$fileName.$JVNSEGMENTER_OUTPUT_FILES_EXTENSION"

            val actualLines = fileReader.read(actualOutputFileFullPath)
            val expectedLines = fileReader.read(expectedOutputFileFullPath)

            val score = evaluator.evaluate(actualLines, expectedLines)
            println("Piece Score: " + score)
            totalScore += score
        }

        println("Average Score: " + (totalScore / fileAmount))
    }
}
