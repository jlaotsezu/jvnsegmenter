package nhom10.filereader

import java.io.File
import java.nio.file.Files

class FileReaderImpl : FileReader {
    override fun read(fullPath: String): List<String> {
        return Files.readAllLines(File(fullPath).toPath())
    }
}