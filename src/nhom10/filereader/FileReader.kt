package nhom10.filereader

interface FileReader{
    fun read(fullPath: String): List<String>
}