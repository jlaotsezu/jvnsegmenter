package nhom10.converters

class FromTeacherToRawTextConverterImpl : FromTeacherToRawTextConverter {
    override fun convert(lines: List<String>): List<String> {
        return lines.filterNot {line ->
            line.isXMLTag()
        }
            .map {line ->
                line.removeDateTimeException()
            }
            .map {line ->
                line.replace("_", " ")
            }
    }
}