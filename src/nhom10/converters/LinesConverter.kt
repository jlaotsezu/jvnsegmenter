package nhom10.converters

interface LinesConverter{
    fun convert(lines: List<String>): List<String>
}