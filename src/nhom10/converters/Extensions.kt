package nhom10.converters

fun String.isXMLTag(): Boolean{
    return this.matches(Regex("<.*?>"))
}

fun String.removeDateTimeException(): String{
    var result = this
    result = result.replace("_/_", "/")
    result = result.replace("_-_", "-")
    result = result.replace("_:_", ":")
    return result
}
fun String.isSignal(): Boolean{
    return this.matches(Regex("[.,\":“”()?*-]"))
}