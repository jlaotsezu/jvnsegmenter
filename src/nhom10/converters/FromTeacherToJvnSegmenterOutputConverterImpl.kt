package nhom10.converters

class FromTeacherToJvnSegmenterOutputConverterImpl : FromTeacherToJvnSegmenterOutputConverter {
    override fun convert(lines: List<String>): List<String> {
        return lines.filterNot {line ->
            line.isXMLTag()
        }
            .map {line ->
                line.removeDateTimeException()
            }
            .map{line ->
                wrapBracket(line)
            }.map { line ->
                line.replace("_", " ")
            }
    }

    private fun wrapBracket(line: String): String {
        return line.split(" ").joinToString(" ") { word ->
            if (word.isSignal())
                word
            else
                "[$word]"
        }
    }
}