package nhom10

val FILES_NAME = listOf(
        "42267",
        "42298",
        "42390",
        "42402",
        "43389",
        "43427",
        "43435",
        "43463",
        "43478",
        "43484",
        "43488"
)
const val TEACHER_FILES_PATH = "teacherfiles"
const val JVNSEGMENTER_FILES_PATH = "samples/inputdir"
const val PATH_SEPARATOR = "/"
const val TEACHER_FILES_EXTENSION = "seg"
const val JVNSEGMENTER_INPUT_FILES_EXTENSION = "tkn"
const val JVNSEGMENTER_OUTPUT_FILES_EXTENSION = "tkn.wseg"