package nhom10.filewriter

import java.io.File
import java.nio.file.Files

class FileWriterImpl : FileWriter {
    override fun writeLines(lines: List<String>, fullPath: String) {
        val file = File(fullPath)
        if(!file.exists())
            file.createNewFile()
        Files.write(file.toPath(), lines)
    }
    override fun write(content: String, fullPath: String) {
        val file = File(fullPath)
        if(!file.exists())
            file.createNewFile()
        Files.write(file.toPath(), content.toByteArray())
    }
}