package nhom10.filewriter

interface FileWriter {
    fun writeLines(lines: List<String>, fullPath: String)
    fun write(content: String, fullPath: String)
}