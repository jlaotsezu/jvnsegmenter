package nhom10.evaluator

class EvaluatorImpl : Evaluator {
    override fun evaluate(actualLines: List<String>, expectedLines: List<String>): Float {
        val actualWords= actualLines.flatMap {line ->
            line.split(" ")
        }
        val expectedWords = expectedLines.flatMap {line ->
            line.split(" ")
        }

        val tp = calculateTP(actualWords, expectedWords).toFloat()
        val fp = calculateFP(actualWords, expectedWords).toFloat()

        val precision = tp / (tp + fp)

        val fn = calculateFN(actualWords, expectedWords).toFloat()

        val recall = tp / (tp + fn)
        val score = 2 * recall  * precision  / (recall + precision)

        return score
    }

    private fun calculateFN(actualWords: List<String>, expectedWords: List<String>): Int {
        return actualWords.filterNot {word ->
            expectedWords.contains(word)
        }.size
    }

    private fun calculateTP(actualWords: List<String>, expectedWords: List<String>): Int {
        return expectedWords.filter {word ->
            actualWords.contains(word)
        }.size
    }

    private fun calculateFP(actualWords: List<String>, expectedWords: List<String>): Int {
        return expectedWords.filter {word ->
            !actualWords.contains(word)
        }.size
    }
}