package nhom10.evaluator

interface Evaluator{
    fun evaluate(actualLines: List<String>, expectedLines: List<String>): Float
}