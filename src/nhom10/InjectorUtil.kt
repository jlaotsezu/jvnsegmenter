package nhom10

import nhom10.converters.FromTeacherToJvnSegmenterOutputConverter
import nhom10.converters.FromTeacherToJvnSegmenterOutputConverterImpl
import nhom10.evaluator.Evaluator
import nhom10.filereader.FileReader
import nhom10.filewriter.FileWriter
import nhom10.converters.FromTeacherToRawTextConverter
import nhom10.converters.FromTeacherToRawTextConverterImpl
import nhom10.evaluator.EvaluatorImpl
import nhom10.filereader.FileReaderImpl
import nhom10.filewriter.FileWriterImpl

interface InjectorUtil {
    fun fromJvnSegmenterToRawTextConverter(): FromTeacherToRawTextConverter
    fun getEvaluator(): Evaluator
    fun getFileReader(): FileReader
    fun getFileWriter(): FileWriter
    fun fromTeacherToJvnSegmenterOutputConverter(): FromTeacherToJvnSegmenterOutputConverter

    companion object {
        @Volatile
        private var instance: InjectorUtil ?= null
        fun getInstance(): InjectorUtil = instance ?: synchronized(this){
            instance ?: InjectorUtilImpl().also {
                instance = it
            }
        }
    }
}
private class InjectorUtilImpl: InjectorUtil{
    override fun fromJvnSegmenterToRawTextConverter(): FromTeacherToRawTextConverter {
        return FromTeacherToRawTextConverterImpl()
    }

    override fun getEvaluator(): Evaluator {
        return EvaluatorImpl()
    }

    override fun getFileReader(): FileReader {
        return FileReaderImpl()
    }

    override fun getFileWriter(): FileWriter {
        return FileWriterImpl()
    }

    override fun fromTeacherToJvnSegmenterOutputConverter(): FromTeacherToJvnSegmenterOutputConverter {
        return FromTeacherToJvnSegmenterOutputConverterImpl()
    }

}